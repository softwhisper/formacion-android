package es.sw.formacion.testing;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import es.sw.formacion.testing.api.ApiUser;
import es.sw.formacion.testing.mapper.UserMapper;
import es.sw.formacion.testing.model.User;

import static org.junit.Assert.*;

public class UserMapperTest {

    private final static String NAME = "Pepe";
    private final static String SURNAME = "Jimenez";
    private final static String JOB = "Developer";
    private final static String COUNTRY = "Spain";

    private User mUser;

    @Before
    public void init() {

        UserMapper mapper = new UserMapper();
        mUser = mapper.map(mockUser());
    }

    @Test
    public void userMapper_shouldBeAUser() {

        assertThat(mUser, Matchers.is(Matchers.instanceOf(User.class)));
    }

    @Test
    public void userMapper_shouldMapFullName() {

        assertEquals(mUser.getFullName(), NAME + " " + SURNAME);
    }

    @Test
    public void userMapper_shouldMapJob() {

        assertEquals(mUser.getJob(), JOB);
    }

    private ApiUser mockUser() {

        return new ApiUser(NAME, SURNAME, JOB, COUNTRY);
    }
}
