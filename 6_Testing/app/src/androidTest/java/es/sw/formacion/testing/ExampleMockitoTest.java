package es.sw.formacion.testing;

import android.content.Context;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by Juan Porta Trinidad on 27/5/18.
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleMockitoTest {

    private static final String FAKE_STRING = "HELLO WORLD";

    @Mock
    Context mMockContext;

    @Test
    public void readStringFromContext_LocalizedString() {

        // Given a mocked Context injected into the object under test...
        Context context = Mockito.mock(Context.class);

        Mockito.when(context.getString(R.string.app_name)).thenReturn(FAKE_STRING);
        StringUtils stringUtils = new StringUtils(context);

        String result = stringUtils.getAppName();

        Assert.assertEquals(result, FAKE_STRING);
    }

}
