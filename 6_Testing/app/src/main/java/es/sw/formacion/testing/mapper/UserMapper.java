package es.sw.formacion.testing.mapper;

import es.sw.formacion.testing.api.ApiUser;
import es.sw.formacion.testing.model.User;

public class UserMapper {

    public User map(ApiUser apiUser) {

        if (apiUser == null) {

            return null;
        }

        return new User(apiUser.getName() + " " + apiUser.getSurname(), apiUser.getJob());
    }
}
