package es.sw.formacion.testing;

import android.content.Context;

/**
 * Created by Juan Porta Trinidad on 31/5/18.
 */
public class StringUtils {

    private Context mContext;

    public StringUtils(Context context) {

        mContext = context;
    }

    public String getAppName() {

        return mContext.getString(R.string.app_name);
    }
}
