package es.sw.formacion.testing.api;

public class ApiUser {

    private String name;
    private String surname;
    private String job;
    private String country;

    public ApiUser(String name,
                   String surname,
                   String job,
                   String country) {

        this.name = name;
        this.surname = surname;
        this.job = job;
    }

    public String getName() {

        return name;
    }

    public String getSurname() {

        return surname;
    }

    public String getJob() {

        return job;
    }

    public String getCountry() {

        return country;
    }
}
