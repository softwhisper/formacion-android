package es.sw.formacion.testing.model;

public class User {

    private String fullName;
    private String job;

    public User(String fullName,
                String job) {

        this.fullName = fullName;
        this.job = job;
    }

    public String getFullName() {

        return fullName;
    }

    public String getJob() {

        return job;
    }
}
