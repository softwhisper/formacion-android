package es.sw.formacion.echo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class EchoActivity extends AppCompatActivity {

    private static final String EXTRA_ECHO = "echo";

    public static Intent newIntent(Context context, String echo) {

        Intent intent = new Intent(context, EchoActivity.class);
        intent.putExtra(EXTRA_ECHO, echo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_echo);

        String echo = getIntent().getExtras().getString(EXTRA_ECHO);

        TextView textView = findViewById(R.id.tv_content_main);
        textView.setText(echo);
    }
}
