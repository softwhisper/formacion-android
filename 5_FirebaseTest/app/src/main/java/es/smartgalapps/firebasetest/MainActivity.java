package es.smartgalapps.firebasetest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import es.smartgalapps.firebasetest.utils.AnalyticsNames;
import es.smartgalapps.firebasetest.utils.RemoteConfigKeys;

public class MainActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private InterstitialAd mInterstitialAd;

    private boolean mShowInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");
        MobileAds.initialize(this, "ca-app-pub-3940256099942544");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG).build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "fab_main_activity");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Floating Action Button");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "on click");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                Toast.makeText(MainActivity.this, "Enviando evento", Toast.LENGTH_SHORT).show();

//                Crashlytics.getInstance().crash(); // Force a crash

                if (mShowInterstitialAd) {

                    if (mInterstitialAd.isLoaded()) {

                        mInterstitialAd.show();

                    } else {

                        Log.d("TAG", "The interstitial wasn't loaded yet.");
                    }

                } else {

                    Log.d("TAG", "The interstitial is not show because remote config.");
                    launchNextActivity();
                }
            }
        });

        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");

        ViewGroup adViewLayout = findViewById(R.id.ad_view_layout);
        adViewLayout.addView(adView);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {

                launchNextActivity();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

                launchNextActivity();
            }
        });

        mShowInterstitialAd = mFirebaseRemoteConfig.getBoolean(RemoteConfigKeys.SHOW_INTERSTITIAL);
        fetchRemoteConfig();
    }

    @Override
    protected void onResume() {

        super.onResume();
        // Note that screen reporting is enabled automatically and records the class name of the current Activity for you without requiring you to call this function.
        // The class name can optionally be overridden by calling this function in the onResume callback of your Activity and specifying the screenClassOverride parameter.
        mFirebaseAnalytics.setCurrentScreen(this, AnalyticsNames.SC_SECOND_ACTIVITY_NAME, null);
    }

    private void launchNextActivity() {

        startActivity(SecondActivity.newIntent(this));
    }

    private void fetchRemoteConfig() {

        long cacheExpiration = 3600; // 1 hour in seconds.
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {

            cacheExpiration = 0;
        }

        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
        // will use fetch data from the Remote Config service, rather than cached parameter values,
        // if cached parameter values are more than cacheExpiration seconds old.
        // See Best Practices in the README for more information.
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {

                            Toast.makeText(MainActivity.this, "Fetch Succeeded", Toast.LENGTH_SHORT)
                                    .show();

                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();

                        } else {

                            Toast.makeText(MainActivity.this, "Fetch Failed", Toast.LENGTH_SHORT)
                                    .show();
                        }

                        mShowInterstitialAd = mFirebaseRemoteConfig
                                .getBoolean(RemoteConfigKeys.SHOW_INTERSTITIAL);
                    }
                });
    }
}
