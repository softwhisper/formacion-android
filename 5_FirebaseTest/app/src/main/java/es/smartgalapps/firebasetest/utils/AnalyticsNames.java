package es.smartgalapps.firebasetest.utils;

/**
 * Created by Juan Porta Trinidad on 21/5/18.
 */
public class AnalyticsNames {

    // Screen Names
    public static final String SC_MAIN_ACTIVITY_NAME = "Pantalla principal";
    public static final String SC_SECOND_ACTIVITY_NAME = "Pantalla secundaria";
    public static final String SC_REFINERIES_NAME = "Pantalla de refinerías";

}
