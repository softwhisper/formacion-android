package es.smartgalapps.firebasetest.utils;

/**
 * Created by Juan Porta Trinidad on 21/5/18.
 */
public class RemoteConfigKeys {

    public static final String SECOND_ACTIVITY_WELCOME_MESSAGE = "welcome_message";
    public static final String SHOW_INTERSTITIAL = "show_interstitial";
}
