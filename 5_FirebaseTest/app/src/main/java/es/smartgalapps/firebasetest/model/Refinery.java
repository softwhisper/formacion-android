package es.smartgalapps.firebasetest.model;

/**
 * Created by Juan Porta Trinidad on 21/5/18.
 */
public class Refinery {

    private String id;
    private String name;
    private Double lat;
    private Double lon;
    private String city;

    Refinery(String id,
             String name,
             Double lat,
             Double lon,
             String city) {

        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.city = city;
    }

    public String getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public Double getLat() {

        return lat;
    }

    public Double getLon() {

        return lon;
    }

    public String getCity() {

        return city;
    }

    public static class Builder {

        private String id;
        private String name;
        private Double lat;
        private Double lon;
        private String city;

        public Builder setId(String id) {

            this.id = id;
            return this;
        }

        public Builder setName(String name) {

            this.name = name;
            return this;
        }

        public Builder setLat(Double lat) {

            this.lat = lat;
            return this;
        }

        public Builder setLon(Double lon) {

            this.lon = lon;
            return this;
        }

        public Builder setCity(String city) {

            this.city = city;
            return this;
        }

        public Refinery build() {

            return new Refinery(id, name, lat, lon, city);
        }
    }
}
