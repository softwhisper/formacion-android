package es.smartgalapps.firebasetest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.smartgalapps.firebasetest.R;
import es.smartgalapps.firebasetest.model.Refinery;

/**
 * Created by Juan Porta Trinidad on 21/5/18.
 */
public class RefineriesAdapter extends RecyclerView.Adapter<RefineriesAdapter.ViewHolder> {

    private Context mContext;
    private List<Refinery> mItems;

    public RefineriesAdapter(Context context) {

        mContext = context;
        mItems = new ArrayList<>();
    }

    @Override
    public RefineriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {

        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_refinery, parent, false);
        return new ViewHolder(mContext, v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,
                                 int position) {

        holder.bind(mItems.get(position));
    }

    @Override
    public int getItemCount() {

        return mItems.size();
    }

    public void setItems(List<Refinery> items) {

        mItems = items;
        notifyDataSetChanged();
    }

    public void addItem(Refinery item) {

        mItems.add(item);
        notifyItemInserted(mItems.size() - 1);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;
        private RelativeLayout mContentLayout;

        ViewHolder(Context context,
                   RelativeLayout contentLayout) {

            super(contentLayout);
            mContext = context;
            mContentLayout = contentLayout;
        }

        void bind(Refinery refinery) {

            TextView tvName = mContentLayout.findViewById(R.id.tv_row_refinery_name);
            tvName.setText(refinery.getName());

            TextView tvCity = mContentLayout.findViewById(R.id.tv_row_refinery_city);
            tvCity.setText(refinery.getCity());

            TextView tvCoords = mContentLayout.findViewById(R.id.tv_row_refinery_coords);
            tvCoords.setText(mContext
                    .getString(R.string.coords, refinery.getLat(), refinery.getLon()));

        }
    }
}
