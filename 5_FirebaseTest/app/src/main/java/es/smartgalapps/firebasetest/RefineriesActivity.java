package es.smartgalapps.firebasetest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import es.smartgalapps.firebasetest.adapters.RefineriesAdapter;
import es.smartgalapps.firebasetest.model.Refinery;
import es.smartgalapps.firebasetest.utils.AnalyticsNames;

/**
 * Created by Juan Porta Trinidad on 21/5/18.
 */
public class RefineriesActivity extends AppCompatActivity {

    private static final String TAG = RefineriesActivity.class.getSimpleName();

    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private FirebaseFirestore mFirestore;

    private RecyclerView mList;
    private RefineriesAdapter mAdapter;

    public static Intent newIntent(Context context) {

        return new Intent(context, RefineriesActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refineries);

        mList = findViewById(R.id.rv_refineries);

        mFirestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true).build();
        mFirestore.setFirestoreSettings(settings);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mList.setLayoutManager(layoutManager);

        mAdapter = new RefineriesAdapter(this);
        mList.setAdapter(mAdapter);

        // TODO Borrar ya que con el EventListener ya hace también la primera llamada
        //        mFirestore.collection("refineries").get()
        //                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
        //
        //                    @Override
        //                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        //
        //                        if (task.isSuccessful()) {
        //
        //                            createRefineries(task);
        //
        //                        } else {
        //
        //                            Log.w(TAG, "Error getting documents.", task.getException());
        //                        }
        //                    }
        //                });

        final CollectionReference docRef = mFirestore.collection("refineries");
        docRef.addSnapshotListener(new EventListener<QuerySnapshot>() {

            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                @Nullable FirebaseFirestoreException e) {

                if (queryDocumentSnapshots != null) {

                    createRefineries(queryDocumentSnapshots.getDocuments());
                }
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
        mFirebaseAnalytics.setCurrentScreen(this, AnalyticsNames.SC_SECOND_ACTIVITY_NAME, null);
    }

    private void createRefineries(List<DocumentSnapshot> documents) {

        List<Refinery> refineries = new ArrayList<>();

        for (DocumentSnapshot document : documents) {

            Log.d(TAG, document.getId() + " => " + document.getData());
            Refinery refinery = getRefinery(document.getId(), document.getData());

            if (refinery != null) {

                refineries.add(refinery);
            }
        }

        mAdapter.setItems(refineries);
    }

    private Refinery getRefinery(String id,
                                 Map<String, Object> data) {

        if (data != null) {

            Refinery.Builder builder = new Refinery.Builder();
            builder.setId(id);
            builder.setName((String) data.get("name"));
            builder.setCity((String) data.get("city"));
            builder.setLat((Double) data.get("lat"));
            builder.setLon((Double) data.get("lon"));
            return builder.build();
        }

        return null;
    }
}
