package es.smartgalapps.firebasetest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import es.smartgalapps.firebasetest.utils.AnalyticsNames;
import es.smartgalapps.firebasetest.utils.RemoteConfigKeys;

/**
 * Created by Juan Porta Trinidad on 21/5/18.
 */
public class SecondActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private TextView mTvMessage;

    public static Intent newIntent(Context context) {

        return new Intent(context, SecondActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mTvMessage = findViewById(R.id.tv_message_second_activity);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        showWelcomeMessage();

        findViewById(R.id.bt_second_activity).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                startActivity(RefineriesActivity.newIntent(SecondActivity.this));
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
        mFirebaseAnalytics.setCurrentScreen(this, AnalyticsNames.SC_SECOND_ACTIVITY_NAME, null);
    }

    private void showWelcomeMessage() {

        String welcomeMessage = mFirebaseRemoteConfig
                .getString(RemoteConfigKeys.SECOND_ACTIVITY_WELCOME_MESSAGE);
        mTvMessage.setText(welcomeMessage);
    }
}
